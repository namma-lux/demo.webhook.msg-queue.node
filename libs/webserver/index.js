// use strict

const express = require('express');
const queue = require('../queue');
const mapper = require('../event-action-mapper');


const HEADER_KEY_EVENT     = 'x-event';
const HEADER_KEY_SIGNATURE = 'x-signature';


exports.createWebServer = async function (host, port) {

    const publisher = await queue.factory.createPublisher();

    const app = express();
    configMiddlewares(app);
    configRoutes(app, publisher);

    const server = await new Promise((resolve, reject) => {
        const newServerInstance = app.listen(port, host,
            err => err
                ? reject(err)
                : resolve(newServerInstance));
    });

    return {
        close: server.close.bind(server)
    };
}


function configMiddlewares(app) {
    app.use(express.json());
}


function configRoutes(app, publisher) {
    app.post('/webhook',
        async (req, resp) => {
            const event     = req.headers[HEADER_KEY_EVENT];
            const signature = req.headers[HEADER_KEY_SIGNATURE];
            const payload   = req.body;

            // VALIDATION

            if (eventNotProvided = !event)
                return sendSimpleResponseForMissingHeader(resp, HEADER_KEY_EVENT);
            if (signatureNotProvided = !signature)
                return sendSimpleResponseForMissingHeader(resp, HEADER_KEY_SIGNATURE);

            if (signatureNotAuthenticated = !authenticate(signature))
                return sendSimpleResponse(resp, 401, 'Invalid signature');

            if (eventNotAvailable = !mapper.hasEvent(event))
                return sendSimpleResponse(resp, 400, `Event [${event}] not available`);

            const validate = mapper.getRequestValidator(event);
            if (errmsg = await validate(payload))
                return sendSimpleResponse(resp, 400, errmsg);

            // JOB PUBLISHING
            try {
                await publisher.push(event, payload);
                console.log(`> New event [${event}] has been pushed into queue with payload`, payload);
                return sendSimpleResponse(resp, 200, 'OK');

            } catch (err) {
                resolveError(err);
                return sendSimpleResponse(resp, 500, 'Server error');
            }
        });
}


function authenticate(signature) {
    // TODO Validate the signature; return [false] if invalid.

    return true;
}

function sendSimpleResponse(resp, statusCode, message) {
    resp.writeHead(statusCode, message);
    return resp.send();
}

function sendSimpleResponseForMissingHeader(resp, key) {
    return sendSimpleResponse(resp, 400, `Request header [${key}] is required`);
}

function resolveError(err) {
    console.error(err);
}