// use strict

exports.handleJobForMessage = async function (payload) {
    console.log('> Received a new message:', payload.message);
};

exports.handleJobForImage = async function (payload) {
    console.log('> Received new image; access with URL:', payload.imageUrl);
};

exports.handleJobForNewFriend = async function ({ userId1, userId2 }) {
    console.log(`> New user connnection for #${userId1} and #${userId2}`);
};

exports.handleJobForDeactivatedUser = async function (payload) {
    console.log(`> Deactivated a user who has User ID #${payload.userId}`);
};
