// use strict

exports.validateRequestForMessage = async function (payload) {
    if (payload.message == undefined)
        return `[message] is required`;
}

exports.validateRequestForImage = async function (payload) {
    if (payload.imageUrl == undefined)
        return '[imageUrl] is required';
}

exports.validateRequestForNewFriend = async function (payload) {
    const requiredKeys = ['userId1', 'userId2'];
    for (const key of requiredKeys)
        if (payload[key] == undefined)
            return `[${key}] is required`;
}

exports.validateRequestForDeactivatedUser = async function (payload) {
    if (payload.userId == undefined)
        return '[userId] is required';
}
