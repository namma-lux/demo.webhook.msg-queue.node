// use strict

const { Queue, Worker } = require('bullmq');
const { QUEUE_NAME } = require('./const');
const { JobNameNotAvailableError } = require('./errors');
const mapper = require('../event-action-mapper');


exports.createPublisher = async function () {
    const q = new Queue(QUEUE_NAME);

    const publisher = {
        disconnect: q.disconnect.bind(q),
        push      : (jobName, payload) => q.add(jobName, payload),
    };

    return publisher;
}

exports.createSubscriber = async function () {
    const worker = new Worker(QUEUE_NAME, processor);

    const subscriber = {
        disconnect: async () => await worker.disconnect(),
    };

    return subscriber;
}


async function processor (job) {
    const event = job.name;

    if (eventNotAvailable = !mapper.hasEvent(event))
        throw new JobNameNotAvailableError(event);

    try {
        return await mapper
            .getHandler(event)
            .call(mapper, payload = job.data);

    } catch (err) {
        // Do something like logging on console
        console.error(err);

        // or sending to Slack
        // TODO logErrorOnSlack(err);

        // then, MUST re-throw the error
        // so that the job would be marked as failed.
        throw err;
    }
}