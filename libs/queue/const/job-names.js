// use strict

exports.MESSAGE_SENT     = 'message-sent';
exports.IMAGE_UPLOADED   = 'image-uploaded';
exports.USER_ADDED       = 'user-added';
exports.USER_DEACTIVATED = 'user-deactivated';
