// use strict

module.exports = class JobNameNotAvailableError extends Error {
    constructor (jobName) {
        super(`Job name [${jobName}] not available`);
    }
}
