// use strict

const { JOB_NAMES } = require('./queue/const');
const requestValidators = require('./request-validators');
const { jobHandler: handler } = require('./services');


const map = {

    [JOB_NAMES.MESSAGE_SENT]: {
        requestValidator: requestValidators.validateRequestForMessage,
        handler         : handler.handleJobForMessage,
    },

    [JOB_NAMES.IMAGE_UPLOADED]: {
        requestValidator: requestValidators.validateRequestForImage,
        handler         : handler.handleJobForImage,
    },

    [JOB_NAMES.USER_ADDED]: {
        requestValidator: requestValidators.validateRequestForNewFriend,
        handler         : handler.handleJobForNewFriend,
    },

    [JOB_NAMES.USER_DEACTIVATED]: {
        requestValidator: requestValidators.validateRequestForDeactivatedUser,
        handler         : handler.handleJobForDeactivatedUser,
    },

};

exports.getEventNames = () => Object.keys(map);

exports.hasEvent = event => this.getEventNames().includes(event);

exports.getRequestValidator = event => map[event] && map[event].requestValidator;
exports.getHandler          = event => map[event] && map[event].handler;
