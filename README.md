### Environment Configuration

1. Install and run **Redis** _(including Redis server, and Redis CLI)_.
2. Run **web-server** _(for receiving remote-event, and **pushing** the event into the queue)_.
3. Run **worker** _(for **pulling** the event from the queue and handle)_.
4. Send a POST message to `http://localhost:3000/webhook` with requirements listed below.

### Install and run Redis

1. Install:
>`brew install redis`
2. Run:
>`brew start redis`
3. Stop:
>`brew stop redis`


### Run web server

>`./bin/webserver`


### Run worker

>`./bin/subscriber`


### Common requirements for every requests

Every request **MUST** have the following headers:

1. `Content-Type`:`application/json`,
2. `x-signature`: just random string because the signature testing is not yet implemented,
3. `x-event`: as specific requirements below.

### Specific requirements

#### `x-event` : `message-sent`
    Payload need 1 key: [message].

#### `x-event` : `image-uploaded`
    Payload need 1 key: [imageUrl].

#### `x-event` : `user-uploaded`
    Payload need 2 key: [userId1], [userId2].

#### `x-event` : `user-deactivated`
    Payload need 1 key: [userId].
